import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt

#
# VIDEO TIME
#

# Dataset from keras
data = keras.datasets.fashion_mnist

# Split dataset
(train_images, train_labels), (test_images, test_labels) = data.load_data()

# Dataset label names
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

# Value out of 1
train_images = train_images / 255.0
test_images = test_images / 255.0

# Define model
model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(128, activation="relu"),
    keras.layers.Dense(10, activation="softmax")
])

model.compile(optimizer="adam",
              loss="sparse_categorical_crossentropy", metrics=["accuracy"])

# Train the model
model.fit(train_images, train_labels, epochs=8)

# Array of predictions
prediction = model.predict(test_images)

# Show 5 images
for image in range(5):
    plt.grid(False)
    plt.imshow(test_images[image], cmap=plt.cm.binary)
    plt.xlabel("Actual: " + class_names[test_labels[image]])
    plt.title("Prediction: " + class_names[np.argmax(prediction[image])])
    plt.show()
