import tensorflow as td
from tensorflow import keras
import numpy as np

# Pad max length
ml = 256

# Imdb dataset
data = keras.datasets.imdb

# Train and test datas
(train_data, train_labels), (test_data,
                             test_labels) = data.load_data(num_words=10000)

print("Length of train data & labels " +
      str(len(train_data)) + " " + str(len(train_labels)))
print("Length of test data & labels " +
      str(len(test_data)) + " " + str(len(test_labels)))

# Get words from word indexes
word_index = data.get_word_index()
word_index = {key: (value+3) for key, value in word_index.items()}
# Helpers for model
word_index["<PAD>"] = 0
word_index["<START>"] = 1
word_index["<UNK>"] = 2
word_index["<UNUSED>"] = 3

# Swap key and value positions
reverse_word_index = dict([(value, key)
                          for (key, value) in word_index.items()])

# Normalize data to 256 words
train_data = keras.preprocessing.sequence.pad_sequences(
    train_data, value=word_index["<PAD>"], padding="post", maxlen=ml)
test_data = keras.preprocessing.sequence.pad_sequences(
    test_data, value=word_index["<PAD>"], padding="post", maxlen=ml)


# Returns words in human readable form
def decode_review(text):
    return " ".join([reverse_word_index.get(i, "?") for i in text])


# If the model is already saved
model = keras.models.load_model("imdb-model.h5")

'''
# Define model
model = keras.Sequential()
model.add(keras.layers.Embedding(88000, 16))
model.add(keras.layers.GlobalAveragePooling1D())
model.add(keras.layers.Dense(16, activation="relu"))
model.add(keras.layers.Dense(1, activation="sigmoid"))

model.summary()

model.compile(optimizer="adam", loss="binary_crossentropy",
              metrics=["accuracy"])

x_val = train_data[:10000]
x_train = train_data[:10000]

y_val = train_labels[:10000]
y_train = train_labels[:10000]

fitModel = model.fit(x_train, y_train, epochs=40,
                     batch_size=512, validation_data=(x_val, y_val), verbose=1)


model.save("imdb-model.h5")
'''

results = model.evaluate(test_data, test_labels)

# Test the ith review

i = 2

test_review = test_data[i]
predict = model.predict([test_review])
print("Review: ")
print(decode_review(test_review))
print("Prediction: " + str(predict[i]))
print("Actual: " + str(test_labels[i]))
print(results)
