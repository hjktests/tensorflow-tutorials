# Tensorflow tutorials

Testing out Tensorflow & Python. Followed [this video](https://youtu.be/tZt6gRlRcgk) to learn.

Tutorials:

- [https://www.tensorflow.org/tutorials/keras/classification](https://www.tensorflow.org/tutorials/keras/classification)

## Setup environment

```shell
pipenv install
```

```shell
pipenv shell
```

```shell
pipenv run python FILENAME_TO_RUN.py
```
